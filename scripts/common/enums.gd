extends Node
class_name Enums


enum SortColor { RED, GREEN, BLUE } 

static func enum_to_color(sort_color: SortColor) -> Color:
	match sort_color:
		SortColor.RED : return Color.RED
		SortColor.GREEN : return Color.GREEN
		SortColor.BLUE : return Color.BLUE
		_ : 
			push_error("Not handled color!")
			return 0


