extends StaticBody2D
class_name Envelope


@export var _visual : Sprite2D

var _own_color : Enums.SortColor
var _path : ConveyorPath


func initialize(own_color: Enums.SortColor):
	_own_color = own_color
	_visual.modulate = Enums.enum_to_color(_own_color)


func notify_put_on_conveyor_path(path: ConveyorPath):
	_path = path

func notify_taken():
	_path = null
	
func is_on_proper_conveyor() -> bool:
	if !_path:
		printerr("Envelope is not on a conveyor!")
	return _path.c == _own_color
