extends RefCounted
class_name EnvelopeSpawner


var _envelope_prefab : PackedScene
var _spawn_parent : Node2D
var _path: ConveyorPath


func _init(envelope_prefab : PackedScene, spawn_parent: Node, path: ConveyorPath):
	_envelope_prefab = envelope_prefab
	_spawn_parent = spawn_parent
	_path = path
	
	
func spawn(color : Enums.SortColor):
	var envelope = _envelope_prefab.instantiate() as Envelope
	
	envelope.position.y = _path.top_position_y
	_spawn_parent.add_child(envelope)
	
	envelope.initialize(color)
	_path.put_envelope(envelope)
