#It's an ABSTRACT class
extends RefCounted
class_name SpawnController

var _is_active : bool = false
var _spawners : Array[EnvelopeSpawner]
var _colors : Array[Enums.SortColor]


func _init(colors: Array[Enums.SortColor]):
	_colors = colors


func start_spawning():
	_is_active = true

func stop_spawning():
	_is_active = false
