extends SpawnController
class_name RandomSpawn


var _cooldown_from : float
var _cooldown_to : float

var _tree : SceneTree
var _timer : SceneTreeTimer


func _init(colors: Array[Enums.SortColor], cooldown_from: float, cooldown_to: float, spawners: Array[EnvelopeSpawner], tree : SceneTree):
	super._init(colors)
	_cooldown_from = cooldown_from
	_cooldown_to = cooldown_to
	_spawners = spawners
	_tree = tree
	
	_tree.process_frame.connect(func(): _spawn_coroutine())

func _spawn_coroutine():
	if !_is_active:
		return
	
	if _timer:
		if _timer.time_left == 0:
			_spawn()
			_create_cooldown()
	else:
		_spawn()
		_create_cooldown()


func _create_cooldown():
	var cooldown = randf_range(_cooldown_from, _cooldown_to)
	_timer = _tree.create_timer(cooldown)

func _spawn():
	var spawner = _spawners[randi_range(0, len(_spawners)-1)]
	var color = _colors[randi_range(0, len(_colors)-1)]
	
	spawner.spawn(color)
