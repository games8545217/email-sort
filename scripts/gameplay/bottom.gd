extends Area2D
class_name Bottom


signal match_fall
signal mismatch_fall


func body_entered(body):
	if body is Envelope:
		if body.is_on_proper_conveyor():
			match_fall.emit()
		else:
			mismatch_fall.emit()
