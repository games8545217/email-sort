extends Node2D
class_name EnvelopeDrag

@export_flags_2d_physics var envelopes_layer : int
@export_flags_2d_physics var conveyor_paths_layer : int
@export_flags_2d_physics var conveyors_layer : int

var _envelope : Envelope 
var _last_path : ConveyorPath

func _input(event):
	if event is InputEventMouse and Input.is_action_just_pressed("drag_envelope"):
		var envelope = _try_get_envelope_under_cursor()
		if envelope:
			_take_envelope(envelope)

func _physics_process(delta):
	if _envelope:
		_sync_envelopr_position_with_cursor()

func _try_get_envelope_under_cursor() -> Envelope:
	return _try_get_object_under_cursor(envelopes_layer)

func _try_get_path_under_cursor() -> ConveyorPath:
	return _try_get_object_under_cursor(conveyor_paths_layer)

func _try_get_object_under_cursor(mask: int):
	const COLLIDER_KEY = "collider"
	
	var space_state = get_world_2d().direct_space_state
	var query = PhysicsPointQueryParameters2D.new()
	
	query.collide_with_areas = true
	query.position = get_global_mouse_position()
	query.collision_mask = mask
	
	var under_cursor = space_state.intersect_point(query)
	if under_cursor:
		return under_cursor[0][COLLIDER_KEY]
	else:
		return null
	
func _take_envelope(envelope: Envelope):
	_last_path = _try_get_path_under_cursor()
	_last_path.take_envelope(envelope)
	_envelope = envelope

func _put_envelope(envelope: Envelope, path: ConveyorPath):
	path.put_envelope(envelope)
	_envelope = null
	

func _sync_envelopr_position_with_cursor():
	
	_envelope.global_position.x = get_global_mouse_position().x
	
	if Input.is_action_just_released("drag_envelope"):
	
		var path = _try_get_path_under_cursor()
		if path:
			_put_envelope(_envelope, path)
		else:
			_put_envelope(_envelope, _last_path)
