extends Area2D
class_name ConveyorPath


var top_position_y : float :
	get = _calculate_top_y
var envelope_snap_position_x : float :
	get : return position.x
var width: float:
	get : return _shape.shape.size.x*scale.x


@export var _shape : CollisionShape2D

var _speed : float
var _envelopes : Array[Envelope]
var _color : Enums.SortColor


func initialize(speed: float, size: Vector2, color : Enums.SortColor):
	scale = size
	_speed = speed
	_color = color


func put_envelope(envelope: Envelope):
	_envelopes.append(envelope)

	envelope.notify_put_on_conveyor_path(self)
	envelope.position.x = envelope_snap_position_x

func take_envelope(envelope: Envelope):
	var remove_index = _envelopes.find(envelope)
	if remove_index != -1: #FIXME: Temp check
		_envelopes.remove_at(remove_index)
		envelope.notify_taken()


func _process(delta):
	for env in _envelopes:
		env.position.y += _speed*delta

func _calculate_top_y() -> float:
	return position.y - (scale.y*_shape.shape.size.y)/2
