extends Area2D
class_name Conveyor

@export var _shape : CollisionShape2D
@export var _visual : Sprite2D

var _color : Enums.SortColor


func initialize(color : Enums.SortColor, size: Vector2):
	scale = size
	_color = color
	_visual.modulate = Enums.enum_to_color(color)
	 
