extends Node
class_name LevelBuilder


@export var _path_prefab : PackedScene
@export var _conveyor_prefab : PackedScene
@export var _envelope_prefab : PackedScene

@export var _levels: Array[LevelRES]
@export var _look_settings : LookSettingsRES

@export var _conveyors_parent : Node2D
@export var _envelopes_parent : Node2D
@export var _screen_center : Marker2D

func _ready():
	load_level(3) #FIXME: temp

func load_level(number: int):
	var level_resource = _get_level_by_number(number)
	var paths = _build_paths_and_conveyors(level_resource)
	var spawn_controller = _create_spawn_controller(paths, level_resource)
	spawn_controller.start_spawning()

func _build_paths_and_conveyors(level: LevelRES) -> Array[ConveyorPath]:
	var conveyor_start_spawn_position = Vector2.ZERO
	var path_spawn_position = conveyor_start_spawn_position
	
	var paths : Array[ConveyorPath]
	for conveyor in level.conveyors:
		for _path_number in range(conveyor.paths_amount):
			var path = _spawn_path(path_spawn_position, conveyor)
			path_spawn_position.x += path.width + _look_settings.path_gaps
			
			paths.append(path)
			
		var start = conveyor_start_spawn_position
		var end = Vector2(path_spawn_position.x - _look_settings.path_gaps, path_spawn_position.y)
		
		_spawn_conveyor(start, end, conveyor.color)
		
		path_spawn_position.x += _look_settings.conveyor_gaps
		conveyor_start_spawn_position = path_spawn_position
	
	return paths
	#_allign_to_center(Vector2.ZERO, path_spawn_position)


func _spawn_path(spawn_position: Vector2, conveyor_properties: ConveyorRES) -> ConveyorPath:
	var path = _path_prefab.instantiate() as ConveyorPath
	path.position = spawn_position
	path.initialize(conveyor_properties.fall_speed, _look_settings.path_size, conveyor_properties.color)
	_conveyors_parent.add_child(path)
	
	return path

func _spawn_conveyor(initial_point_px: Vector2, end_point_px: Vector2, color : Enums.SortColor) -> Conveyor:
	const FROM_PX_TO_SIZE = 10
	var paths_center = Vector2(
		_intermidiate_value(initial_point_px.x, end_point_px.x),
		_intermidiate_value(initial_point_px.y, end_point_px.y)
	)
	var conveyor_size = Vector2 (
		(end_point_px.x - initial_point_px.x)/FROM_PX_TO_SIZE,
		_look_settings.path_size.y
	)
	paths_center.x -= (_look_settings.path_size.x/2)*FROM_PX_TO_SIZE
	
	var conveyor = _conveyor_prefab.instantiate() as Conveyor
	_conveyors_parent.add_child(conveyor)
	
	conveyor.initialize(color, conveyor_size)
	conveyor.position = paths_center
	
	return conveyor

func _allign_to_center(start: Vector2, end: Vector2):
	var all_conveyors_center = Vector2(
		_intermidiate_value(start.x, end.x),
		_intermidiate_value(start.y, end.y)
	)
	var delta_align = _screen_center.position - all_conveyors_center
	_conveyors_parent.position += delta_align

func _create_spawn_controller(paths: Array[ConveyorPath], level_properties: LevelRES) -> SpawnController:
	var spawners = _create_spawneres_for_paths(paths)
	var colors = level_properties.used_colors()
	
	return level_properties.spawn_algorithm.create_controller(spawners, colors, get_tree())
	
	
func _create_spawneres_for_paths(paths: Array[ConveyorPath]) -> Array[EnvelopeSpawner]:
	var spawners : Array[EnvelopeSpawner]
	for path in paths:
		var spawner = EnvelopeSpawner.new(_envelope_prefab, _envelopes_parent, path)
		spawners.append(spawner)
	
	return spawners

func _intermidiate_value(a: float, b: float) -> float:
	return a+ (b-a)/2

func _get_level_by_number(number: int) -> LevelRES:
	for level in _levels:
		if level.number == number:
			return level
	
	push_error("No level with such number found")
	return null
