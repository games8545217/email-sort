extends SpawnAlgorithmRES
class_name RandomSpawnAlgorithmRES

@export var cooldown_min : float
@export var cooldown_max : float


func create_controller(spawners: Array[EnvelopeSpawner], colors: Array[Enums.SortColor], tree: SceneTree) -> SpawnController:
	return RandomSpawn.new(colors, cooldown_min, cooldown_max, spawners, tree)
