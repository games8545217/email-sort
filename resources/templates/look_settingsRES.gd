extends Resource
class_name LookSettingsRES
#Class that holds additional settings

@export var path_size : Vector2
@export var path_gaps : float
@export var conveyor_gaps : float
