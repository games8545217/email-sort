extends Resource
class_name LevelRES


@export var conveyors : Array[ConveyorRES]
@export var spawn_algorithm : SpawnAlgorithmRES
@export var number : int


func used_colors() -> Array[Enums.SortColor]:
	var colors : Array[Enums.SortColor]
	for conveyor in conveyors:
		colors.append(conveyor.color)
	
	return colors
	
